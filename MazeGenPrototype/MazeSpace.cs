﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGenPrototype
{
    class MazeSpace
    {
        private Point myPoint;
        private string _type;
        private Maze hostMaze;
        private MazeRoom hostRoom;
        private Guid guid;

        public bool door;

        public int tier_s;
        public int tier_e;

        public MazeSpace(Point p, string type, Maze parentMaze)
        {
            myPoint = p;
            _type = type;
            guid = Guid.NewGuid();
            hostMaze = parentMaze;
            hostRoom = parentMaze.NullRoom;
            tier_s = -1;
            tier_e = -1;
            door = false;
        }

        public MazeSpace(Point p, string type, Maze parentMaze, MazeRoom parentRoom)
        {
            myPoint = p;
            _type = type;
            guid = Guid.NewGuid();
            hostMaze = parentMaze;
            hostRoom = parentRoom;
            tier_s = -1;
            tier_e = -1;
            door = false;
        }

        //public 

        public MazeRoom Room
        {
            get { return hostRoom; }
            set { hostRoom = Room; }
        }

        public Guid ID
        {
            get { return guid; }
        }

        public MazeSpace Left
        {
            get
            {
                if (!myPoint.Left.Null)
                {
                    return hostMaze.Space(myPoint.Left);
                }
                return hostMaze.NullSpace;
            }
        }

        public MazeSpace Right
        {
            get
            {
                if (!myPoint.Right.Null)
                {
                    return hostMaze.Space(myPoint.Right);
                }
                return hostMaze.NullSpace;
            }
        }

        public MazeSpace Up
        {
            get
            {
                if (!myPoint.Up.Null)
                {
                    return hostMaze.Space(myPoint.Up);
                }
                return hostMaze.NullSpace;
            }
        }

        public MazeSpace Down
        {
            get
            {
                if (!myPoint.Down.Null)
                {
                    return hostMaze.Space(myPoint.Down);
                }
                return hostMaze.NullSpace;
            }
        }

        public Maze Maze
        {
            get { return hostMaze; }
            set { hostMaze = Maze; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = Type; }
        }

        public int X
        {
            get { return myPoint.X; }
        }

        public int Y
        {
            get { return myPoint.Y; }
        }

        public Point Point
        {
            get { return myPoint; }
        }

        public bool Root
        {
            get { return DetermineIfRoot(); }
        }

        private bool DetermineIfRoot()
        {
            if (hostRoom.Root.ID == ID)
                return true;
            else if (hostRoom.Spaces.Count == 1)
                return true;
            return false;
        }
    }
}
