﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace MazeGenPrototype
{


    public partial class GenerateView : Form
    {

        public List<string> roomTypes;
        public List<string> wallTypes;
        public List<string> trapTypes;
        public List<string> doorTypes;

        Random rng = new Random();

        //private List<MazeRoom> mazeRooms = new List<MazeRoom>();
        //private List<MazeRoom> halls = new List<MazeRoom>();
        //private List<MazeSpace> traps = new List<MazeSpace>();

        private Maze maze;

        int width;
        int height;
        bool generated = false;

        Bitmap drawArea;

        public GenerateView()
        {
            InitializeComponent();
            drawArea = new Bitmap(drawFrame.Width, drawFrame.Height);
            drawFrame.Image = drawArea;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Graphics g;
            g = Graphics.FromImage(drawArea);

            Pen mypen = new Pen(Brushes.Black);
            g.DrawLine(mypen, 0, 0, 200, 200);
            g.Clear(Color.White);
            g.Dispose();

        }

        

        private void genButton_Click(object sender, EventArgs e)
        {
            bool threwError;
            do
            {
                threwError = false;
                hallMonitor.Items.Clear();
                roomCounter.Text = "0";
                RoomMonitor.Items.Clear();

                weightedRoom[,] rooms = new weightedRoom[7, 7];  //using 7 instead of 6 (max room size) so I can use exact room dimensions instead of having to worry about 0-based array indices.
                rooms = populateRoomArray(rooms);

                width = (int)widthNum.Value;     //get the width and height of the maze from the rollers. set them as globals.
                height = (int)heightNum.Value;

                maze = new Maze(width, height);

                fillMaze(width, height, rooms);   //parse the interior sections of the maze (everything that isn't the indestructable maze boundary) and fill in rooms and halls.
                finishHalls(width, height);
                connectHalls(width, height);

                threwError = addStartFinish(width, height);
                if (threwError)
                    continue;

                addDoors(width, height);

                drawMaze(width, height);

                for (int i = 0; i < maze.Halls.Count; i++)
                {
                    hallMonitor.Items.Add("x: " + maze.Halls[i].Root.X + "; y: " + maze.Halls[i].Root.Y + "; spaces = " + maze.Halls[i].Spaces.Count);
                }
                for (int i = 0; i < maze.Rooms.Count; i++)
                {
                    MazeRoom rm = maze.Rooms[i];
                    RoomMonitor.Items.Add(rm.Width + "x" + rm.Height + "; (" + rm.Root.X + ", " + rm.Root.Y + ")");
                }
                roomCounter.Text = "" + maze.Rooms.Count;
            } while (threwError);
            generated = true;
        }

        private bool addDoors(int width, int height)
        {
            List<List<MazeSpace>> forwardTiers = getForwardTierList();
            List<List<MazeSpace>> backwardTiers = getBackwardTierList();

            bool rev0Tierdooradded = false;
            int tiersToGoal = maze.goal.tier_s;

            for (int i = 0; i < maze.Rooms.Count; i++)
            {
                List<MazeSpace> doorSpaces = maze.Rooms[i].getDoorSpaces();
                doorSpaces = scrambleList(doorSpaces);
                if (maze.Rooms[i].Root.Type == Maze.R_3X3)                  //3x3s only get one door
                {
                    MazeSpace mostDegrees = doorSpaces[0];
                    int totalDegree = mostDegrees.tier_e + mostDegrees.tier_s;
                    bool hadDoor = false;
                    for (int j = 0; j < doorSpaces.Count; j++)
                    {
                        if (doorSpaces[j].door)
                        {
                            maze.Rooms[i].doors.Add(doorSpaces[j]);
                            hadDoor = true;
                            break;
                        }
                        if (doorSpaces[j].tier_e + doorSpaces[j].tier_s > totalDegree)
                            mostDegrees = doorSpaces[j];
                        else if (doorSpaces[j].tier_e + doorSpaces[j].tier_s == totalDegree && Math.Abs(doorSpaces[j].tier_s - doorSpaces[j].tier_e) < Math.Abs(mostDegrees.tier_s - mostDegrees.tier_e))
                            mostDegrees = doorSpaces[j];
                    }
                    if (!hadDoor)
                    {
                        mostDegrees.door = true;
                        maze.Rooms[i].doors.Add(mostDegrees);
                    }
                }
                else
                {
                    List<int> dims = new List<int>();
                    List<int> doordims = new List<int>();
                    for (int j = 0; j < doorSpaces.Count; j++)
                    {
                        MazeSpace currSpace = doorSpaces[j];
                        if (currSpace.door)
                        {
                            maze.Rooms[i].doors.Add(currSpace);
                            doordims.Add(currSpace.tier_s);
                        }
                        if(!dims.Contains(currSpace.tier_s))
                            dims.Add(currSpace.tier_s);
                    }
                    for (int j = 0; j < dims.Count; j++)
                    {
                        if (doordims.Contains(dims[j]))
                            continue;
                        else
                        {
                            
                            MazeSpace mostDegrees = maze.NullSpace;
                            for (int k = 0; k < doorSpaces.Count; k++)
                            {
                                if (doorSpaces[k].tier_s == dims[j])
                                {
                                    mostDegrees = doorSpaces[k];
                                    break;
                                }
                            }
                            doordims.Add(dims[j]);
                            if (mostDegrees != maze.NullSpace)
                            {
                                mostDegrees.door = true;
                                maze.Rooms[i].doors.Add(mostDegrees);
                            }
                        }
                    }
                }
            }
            return false;
        }

        private List<List<MazeSpace>> getForwardTierList()
        {
            List<List<MazeSpace>> tierToStartList = new List<List<MazeSpace>>();
            List<MazeSpace> goalSpaceOnly = new List<MazeSpace>();
            goalSpaceOnly.Add(maze.goal);
            bool allSpacesTiered = false;
            int currentTier = 0;
            List<List<MazeSpace>> tempdoorspacelist = new List<List<MazeSpace>>();
            for (int i = 0; i < maze.Rooms.Count; i++)
            {
                MazeRoom currentRoom = maze.Rooms[i];
                List<MazeSpace> doorSpaces = currentRoom.getDoorSpaces();
                tempdoorspacelist.Add(doorSpaces);
            }
            tempdoorspacelist.Add(goalSpaceOnly);
            while (!allSpacesTiered)
            {
                List<MazeSpace> currentTierList = new List<MazeSpace>();
                for (int i = 0; i < tempdoorspacelist.Count; i++)
                {
                    List<MazeSpace> currUntieredDoors = tempdoorspacelist[i];
                    for (int j = 0; j < currUntieredDoors.Count; j++)
                    {
                        if (currentTier == 0)
                        {
                            if (isDirectPath(currUntieredDoors[j], maze.start))
                            {
                                currUntieredDoors[j].tier_s = currentTier;
                                currentTierList.Add(currUntieredDoors[j]);
                                currUntieredDoors.RemoveAt(j);
                                j--;
                            }
                        }
                        else
                        {
                            List<MazeSpace> prevTierSpaces = tierToStartList[currentTier - 1];
                            for (int k = 0; k < prevTierSpaces.Count; k++)
                            {
                                if (isDirectPath(currUntieredDoors[j], prevTierSpaces[k]))
                                {
                                    currUntieredDoors[j].tier_s = currentTier;
                                    currentTierList.Add(currUntieredDoors[j]);
                                    currUntieredDoors.RemoveAt(j);
                                    j--;
                                    break;
                                }
                            }
                        }
                    }
                }
                tierToStartList.Add(currentTierList);
                int spacesUntiered = 0;
                for (int i = 0; i < tempdoorspacelist.Count; i++)
                {
                    List<MazeSpace> currUntieredDoors = tempdoorspacelist[i];
                    spacesUntiered += currUntieredDoors.Count;
                }
                if (spacesUntiered == 0)
                {
                    allSpacesTiered = true;
                }
                else
                {
                    currentTier++;
                }
            }
            return tierToStartList;
        }

        private List<List<MazeSpace>> getBackwardTierList()
        {
            List<List<MazeSpace>> tierToEndList = new List<List<MazeSpace>>();
            List<MazeSpace> StartSpaceOnly = new List<MazeSpace>();
            StartSpaceOnly.Add(maze.start);
            bool allSpacesTiered = false;
            int currentTier = 0;
            List<List<MazeSpace>> tempdoorspacelist = new List<List<MazeSpace>>();
            for (int i = 0; i < maze.Rooms.Count; i++)
            {
                MazeRoom currentRoom = maze.Rooms[i];
                List<MazeSpace> doorSpaces = currentRoom.getDoorSpaces();
                tempdoorspacelist.Add(doorSpaces);
            }
            tempdoorspacelist.Add(StartSpaceOnly);
            while (!allSpacesTiered)
            {
                List<MazeSpace> currentTierList = new List<MazeSpace>();
                for (int i = 0; i < tempdoorspacelist.Count; i++)
                {
                    List<MazeSpace> currUntieredDoors = tempdoorspacelist[i];
                    for (int j = 0; j < currUntieredDoors.Count; j++)
                    {
                        if (currentTier == 0)
                        {
                            if (isDirectPath(currUntieredDoors[j], maze.goal))
                            {
                                currUntieredDoors[j].tier_e = currentTier;
                                currentTierList.Add(currUntieredDoors[j]);
                                currUntieredDoors.RemoveAt(j);
                                j--;
                            }
                        }
                        else
                        {
                            List<MazeSpace> prevTierSpaces = tierToEndList[currentTier - 1];
                            for (int k = 0; k < prevTierSpaces.Count; k++)
                            {
                                if (isDirectPath(currUntieredDoors[j], prevTierSpaces[k]))
                                {
                                    currUntieredDoors[j].tier_e = currentTier;
                                    currentTierList.Add(currUntieredDoors[j]);
                                    currUntieredDoors.RemoveAt(j);
                                    j--;
                                    break;
                                }
                            }
                        }
                    }
                }
                tierToEndList.Add(currentTierList);
                int spacesUntiered = 0;
                for (int i = 0; i < tempdoorspacelist.Count; i++)
                {
                    List<MazeSpace> currUntieredDoors = tempdoorspacelist[i];
                    spacesUntiered += currUntieredDoors.Count;
                }
                if (spacesUntiered == 0)
                {
                    allSpacesTiered = true;
                }
                else
                {
                    currentTier++;
                }
            }
            return tierToEndList;
        }

        private bool addStartFinish(int width, int height)
        {
            List<MazeSpace> potentialEntEx = new List<MazeSpace>();
            for (int y = 0; y < height; y++)
            {
                MazeSpace left = maze.Space(new Point(0, y));
                MazeSpace right = maze.Space(new Point(width - 1, y));
                if (left.Type == Maze.R_HALL)
                    potentialEntEx.Add(left.Left);
                if (right.Type == Maze.R_HALL)
                    potentialEntEx.Add(right.Right);
            }
            for (int x = 0; x < width; x++)
            {
                MazeSpace up = maze.Space(new Point(x, 0));
                MazeSpace down = maze.Space(new Point(x, height-1));
                if (up.Type == Maze.R_HALL)
                    potentialEntEx.Add(up.Up);
                if (down.Type == Maze.R_HALL)
                    potentialEntEx.Add(down.Down);
            }
            if (potentialEntEx.Count < 2)
                return true;
            potentialEntEx = scrambleList(potentialEntEx);
            bool EntExAdded = false;
            int enterPos = 0;
            int exitPos = 1;
            while (!EntExAdded)
            {
                MazeSpace start = potentialEntEx[enterPos];
                MazeSpace end = potentialEntEx[exitPos];
                if (!isDirectPath(start, end))
                {
                    MazeSpace entrance = new MazeSpace(start.Point, Maze.D_START, maze);
                    maze.addSpace(entrance);
                    MazeSpace exit = new MazeSpace(end.Point, Maze.D_GOAL, maze);
                    maze.addSpace(exit);
                    maze.start = entrance;
                    maze.goal = exit;
                    EntExAdded = true;
                }
                else
                {
                    enterPos++;
                    if (enterPos == exitPos)
                        enterPos++;
                    if (enterPos >= potentialEntEx.Count)
                    {
                        exitPos++;
                        enterPos = 0;
                        if (exitPos >= potentialEntEx.Count)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool isDirectPath(MazeSpace start, MazeSpace end)
        {
            List<MazeSpace> checkSpaces = new List<MazeSpace>();
            List<MazeSpace> alreadyChecked = new List<MazeSpace>();
            MazeSpace curr = start;
            if(maze.roomTypes.Contains(curr.Up.Type))
                checkSpaces.Add(curr.Up);
            if(maze.roomTypes.Contains(curr.Down.Type))
                checkSpaces.Add(curr.Down);
            if(maze.roomTypes.Contains(curr.Left.Type))
                checkSpaces.Add(curr.Left);
            if(maze.roomTypes.Contains(curr.Right.Type))
                checkSpaces.Add(curr.Right);
            while (checkSpaces.Count > 0)
            {
                curr = checkSpaces[0];
                checkSpaces.RemoveAt(0);
                if (alreadyChecked.Contains(curr))
                    continue;
                alreadyChecked.Add(curr);

                if (curr.Up.ID == end.ID || curr.Down.ID == end.ID || curr.Left.ID == end.ID || curr.Right.ID == end.ID)
                    return true;

                if(maze.roomTypes.Contains(curr.Up.Type) && !checkSpaces.Contains(curr.Up) && !alreadyChecked.Contains(curr.Up))
                    checkSpaces.Add(curr.Up);
                if(maze.roomTypes.Contains(curr.Down.Type) && !checkSpaces.Contains(curr.Down) && !alreadyChecked.Contains(curr.Down))
                    checkSpaces.Add(curr.Down);
                if(maze.roomTypes.Contains(curr.Left.Type) && !checkSpaces.Contains(curr.Left) && !alreadyChecked.Contains(curr.Left))
                    checkSpaces.Add(curr.Left);
                if(maze.roomTypes.Contains(curr.Right.Type) && !checkSpaces.Contains(curr.Right) && !alreadyChecked.Contains(curr.Right))
                    checkSpaces.Add(curr.Right);
            }
            return false;
        }

        private bool checkErr(int width, int height)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Point p = new Point(x, y);
                    if (maze.Space(p).Type == Maze.R_EMPTY || maze.Space(p).Type == Maze.R_ERROR || maze.Space(p).Type == Maze.R_FILL)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void addTraps(decimal genChance)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Point p = new Point(x, y);
                    MazeSpace s = maze.Space(p);
                    if (s.Type == Maze.R_HALL)  //traps spawn in hallways
                    {
                        bool l = (s.Left.Type == Maze.R_HALL);
                        bool r = (s.Right.Type == Maze.R_HALL);
                        bool u = (s.Up.Type == Maze.R_HALL);
                        bool d = (s.Down.Type == Maze.R_HALL);

                        if ((l && r && !u && !d) || (!l && !r && u && d))
                        {
                            //trap can spawn here potentially
                            double roll = rng.NextDouble();
                            if (roll <= (double)genChance)
                            {
                                //trap will spawn. roll for which.
                                double roll2 = rng.NextDouble();
                                if (roll2 <= .5)
                                {
                                    //spike trap
                                    maze.addTrap(s, Maze.T_SPIKE);
                                }
                                else
                                {
                                    //steam trap
                                    maze.addTrap(s, Maze.T_STEAM);
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool finishHalls(int width, int height)
        {
            bool error = false;
            for (int y = 0; y<height; y++)
            {
                for (int x = 0; x<width; x++)
                {
                    Point p = new Point(x, y);
                    if (maze.Space(p).Type == Maze.R_FILL)    //if this space is a hall filler
                    {
                        MazeSpace currSpace = new MazeSpace(p, Maze.R_HALL, maze);
                        MazeSpace root = currSpace;
                        List<MazeSpace> consecSpaces = new List<MazeSpace>();
                        List<MazeSpace> checkList = new List<MazeSpace>();
                        int spacesAdded = 0;
                        bool firstIter = true;
                        do
                        {
                            if (firstIter)
                            {
                                firstIter = false;
                            }
                            else
                            {
                                currSpace = new MazeSpace(checkList[0].Point, Maze.R_HALL, maze);
                                checkList.RemoveAt(0);
                            }

                            bool nextToInvalidSpace = false;

                            if (currSpace.Down.Type != Maze.W_STD && currSpace.Down.Type != Maze.W_INV && currSpace.Down.Type != Maze.R_HALL && currSpace.Down.Type != Maze.R_FILL)
                                nextToInvalidSpace = true;
                            if (currSpace.Left.Type != Maze.W_STD && currSpace.Left.Type != Maze.W_INV && currSpace.Left.Type != Maze.R_HALL && currSpace.Left.Type != Maze.R_FILL)
                                nextToInvalidSpace = true;
                            if (currSpace.Right.Type != Maze.W_STD && currSpace.Right.Type != Maze.W_INV && currSpace.Right.Type != Maze.R_HALL && currSpace.Right.Type != Maze.R_FILL)
                                nextToInvalidSpace = true;
                            if (currSpace.Up.Type != Maze.W_STD && currSpace.Up.Type != Maze.W_INV && currSpace.Up.Type != Maze.R_HALL && currSpace.Up.Type != Maze.R_FILL)
                                nextToInvalidSpace = true;

                            if (nextToInvalidSpace)
                            {
                                MazeSpace newWall = new MazeSpace(currSpace.Point, Maze.W_STD, maze);
                                maze.addSpace(newWall);
                            }
                            else
                            {

                                maze.addSpace(currSpace);
                                consecSpaces.Add(currSpace);
                                spacesAdded++;

                                if (currSpace.Left.Type == Maze.R_FILL && !checkList.Contains(currSpace.Left))
                                    checkList.Add(currSpace.Left);
                                if (currSpace.Right.Type == Maze.R_FILL && !checkList.Contains(currSpace.Right))
                                    checkList.Add(currSpace.Right);
                                if (currSpace.Up.Type == Maze.R_FILL && !checkList.Contains(currSpace.Up))
                                    checkList.Add(currSpace.Up);
                                if (currSpace.Down.Type == Maze.R_FILL && !checkList.Contains(currSpace.Down))
                                    checkList.Add(currSpace.Down);

                            }
                        } while (checkList.Count > 0);
                        if (spacesAdded > 0)
                        {
                            MazeRoom h = new MazeRoom(consecSpaces, root);
                            maze.addHall(h);
                        }
                    }
                }
            }
            return error;
        }

        private List<MazeSpace> scrambleList(List<MazeSpace> spc)
        {
            List<MazeSpace> temp = new List<MazeSpace>();
            while (spc.Count > 0)
            {
                int elements = spc.Count;
                int roll = rng.Next(elements);
                temp.Add(spc[roll]);
                spc.RemoveAt(roll);
            }
            return temp;
        }

        private bool connectHalls(int width, int height)
        {
            bool error = false;
            List<MazeSpace> deadEnds = new List<MazeSpace>();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Point p = new Point(x, y);
                    MazeSpace s = maze.Space(p);
                    if (s.Type == Maze.R_HALL)
                    {
                        int touchingHallCount = 0;
                        if (s.Left.Type == Maze.R_HALL)
                            touchingHallCount++;
                        if (s.Right.Type == Maze.R_HALL)
                            touchingHallCount++;
                        if (s.Up.Type == Maze.R_HALL)
                            touchingHallCount++;
                        if (s.Down.Type == Maze.R_HALL)
                            touchingHallCount++;
                        if (touchingHallCount < 2)
                            deadEnds.Add(s);
                    }
                }
            }
            deadEnds = scrambleList(deadEnds);
            while (deadEnds.Count > 0)
            {
                MazeSpace curr = deadEnds[0];
                deadEnds.RemoveAt(0);
                if (curr.Left.Type == Maze.W_STD && curr.Up.Type != Maze.R_HALL && curr.Down.Type != Maze.R_HALL)
                {
                    MazeSpace l = curr.Left;
                    int countsWall = 0;
                    int countsHall = 0;

                    if (l.Up.Left.Type == Maze.W_STD || l.Up.Left.Type == Maze.W_INV || l.Up.Left.Type == Maze.R_HALL)
                        countsWall++;
                    if (l.Left.Type == Maze.W_STD || l.Left.Type == Maze.W_INV)
                        countsWall++;
                    else if (l.Left.Type == Maze.R_HALL)
                    {
                        if (l.Left.Room.ID == curr.Room.ID)
                            continue;
                        else
                            countsHall++;
                    }
                    if (l.Down.Left.Type == Maze.W_STD || l.Down.Left.Type == Maze.W_INV || l.Down.Left.Type == Maze.R_HALL)
                        countsWall++;
                    if (countsWall + countsHall == 3)
                    {
                        MazeSpace newSpace = new MazeSpace(l.Point, Maze.R_HALL, maze, curr.Room);
                        maze.addSpace(newSpace);
                        if (countsHall == 0)
                            deadEnds.Add(newSpace);
                        else
                        {
                            if (newSpace.Left.Type == Maze.R_HALL && newSpace.Left.Room != newSpace.Room)
                            {
                                MazeRoom mergeRoom = newSpace.Left.Room;
                                newSpace.Room.Merge(mergeRoom);
                                maze.deleteHall(mergeRoom);
                            }
                        }
                    }
                }
                if (curr.Up.Type == Maze.W_STD && curr.Left.Type != Maze.R_HALL && curr.Right.Type != Maze.R_HALL)
                {
                    MazeSpace u = curr.Up;
                    int countsWall = 0;
                    int countsHall = 0;
                    if (u.Up.Type == Maze.W_STD || u.Up.Type == Maze.W_INV)
                        countsWall++;
                    else if (u.Up.Type == Maze.R_HALL)
                    {
                        if (u.Up.Room.ID == curr.Room.ID)
                            continue;
                        else
                            countsHall++;
                    }
                    if (u.Up.Left.Type == Maze.W_STD || u.Up.Left.Type == Maze.W_INV || u.Up.Left.Type == Maze.R_HALL)
                        countsWall++;
                    if (u.Up.Right.Type == Maze.W_STD || u.Up.Right.Type == Maze.W_INV || u.Up.Right.Type == Maze.R_HALL)
                        countsWall++;
                    
                    if (countsWall + countsHall == 3)
                    {
                        MazeSpace newSpace = new MazeSpace(u.Point, Maze.R_HALL, maze, curr.Room);
                        maze.addSpace(newSpace);
                        if (countsHall == 0)
                            deadEnds.Add(newSpace);
                        else
                        {
                            if (newSpace.Up.Type == Maze.R_HALL && newSpace.Up.Room != newSpace.Room)
                            {
                                MazeRoom mergeRoom = newSpace.Up.Room;
                                newSpace.Room.Merge(mergeRoom);
                                maze.deleteHall(mergeRoom);
                            }
                        }
                    }
                }
                if (curr.Right.Type == Maze.W_STD && curr.Up.Type != Maze.R_HALL && curr.Down.Type != Maze.R_HALL)
                {
                    MazeSpace r = curr.Right;
                    int countsWall = 0;
                    int countsHall = 0;
                    if (r.Up.Right.Type == Maze.W_STD || r.Up.Right.Type == Maze.W_INV || r.Up.Right.Type == Maze.R_HALL)
                        countsWall++;
                    if (r.Right.Type == Maze.W_STD || r.Right.Type == Maze.W_INV)
                        countsWall++;
                    else if (r.Right.Type == Maze.R_HALL)
                    {
                        if (r.Right.Room.ID == curr.Room.ID)
                            continue;
                        else
                            countsHall++;
                    }
                    if (r.Down.Right.Type == Maze.W_STD || r.Down.Right.Type == Maze.W_INV || r.Down.Right.Type == Maze.R_HALL)
                        countsWall++;

                    if (countsWall + countsHall == 3)
                    {
                        MazeSpace newSpace = new MazeSpace(r.Point, Maze.R_HALL, maze, curr.Room);
                        maze.addSpace(newSpace);
                        if (countsHall == 0)
                            deadEnds.Add(newSpace);
                        else
                        {
                            if (newSpace.Right.Type == Maze.R_HALL && newSpace.Right.Room != newSpace.Room)
                            {
                                MazeRoom mergeRoom = newSpace.Right.Room;
                                newSpace.Room.Merge(mergeRoom);
                                maze.deleteHall(mergeRoom);
                            }
                        }
                    }
                }
                if (curr.Down.Type == Maze.W_STD && curr.Left.Type != Maze.R_HALL && curr.Right.Type != Maze.R_HALL)
                {
                    MazeSpace d = curr.Down;
                    int countsWall = 0;
                    int countsHall = 0;
                    if (d.Down.Type == Maze.W_STD || d.Down.Type == Maze.W_INV)
                        countsWall++;
                    else if (d.Down.Type == Maze.R_HALL)
                    {
                        if (d.Down.Room.ID == curr.Room.ID)
                            continue;
                        else
                            countsHall++;
                    }
                    if (d.Down.Left.Type == Maze.W_STD || d.Down.Left.Type == Maze.W_INV || d.Down.Left.Type == Maze.R_HALL)
                        countsWall++;
                    if (d.Down.Right.Type == Maze.W_STD || d.Down.Right.Type == Maze.W_INV || d.Down.Right.Type == Maze.R_HALL)
                        countsWall++;
                    
                    if (countsWall + countsHall == 3)
                    {
                        MazeSpace newSpace = new MazeSpace(d.Point, Maze.R_HALL, maze, curr.Room);
                        maze.addSpace(newSpace);
                        if (countsHall == 0)
                            deadEnds.Add(newSpace);
                        else
                        {
                            if (newSpace.Down.Type == Maze.R_HALL && newSpace.Down.Room != newSpace.Room)
                            {
                                MazeRoom mergeRoom = newSpace.Down.Room;
                                newSpace.Room.Merge(mergeRoom);
                                maze.deleteHall(mergeRoom);
                            }
                        }
                    }
                }
            }
            /*for (int i = 0; i < maze.Halls.Count; i++)
            {
                if (maze.Halls[i].Spaces.Count == 0)
                {
                    maze.delete
                }
            }*/
            return error;
        }

        private void drawMaze(int width, int height)
        {
            double drawWidth = drawFrame.Width;
            double drawHeight = drawFrame.Height;
            double spaceWidth = drawWidth / (width+2);
            double spaceHeight = drawHeight / (height+2);

            Graphics g = Graphics.FromImage(drawArea);
            Pen p = new Pen(Color.White, 1);
            Brush b = new SolidBrush(Color.Black);
            System.Drawing.Font drawFont = new System.Drawing.Font("Arial", 16);
            Brush txt = new SolidBrush(Color.Black);

            for (int y = 0; y <= height+2; y++)
            {
                for (int x = 0; x <= width+2; x++)
                {
                    string drawStr = "";
                    MazeSpace curr = maze.Space(new Point(x - 1, y - 1));
                    string s = maze.Space(new Point(x-1, y-1)).Type;
                    if (s == Maze.W_INV)
                    {
                        b = new SolidBrush(Color.Black);
                    }
                    else if (s == Maze.W_STD)
                    {
                        if (curr.door)
                            b = new SolidBrush(Color.Lavender);
                        else
                            b = new SolidBrush(Color.FromArgb(40,40,40));
                        if (curr.tier_s != -1 && checkTierS.Checked)
                            drawStr += curr.tier_s + "";
                        if (curr.tier_e != -1 && checkTierE.Checked)
                        {
                            if (drawStr.Length == 1)
                                drawStr += ".";
                            drawStr += curr.tier_e + "";
                        }
                    }
                    else if (s == Maze.R_HALL)
                    {
                        b = new SolidBrush(Color.White);
                    }
                    else if (s == Maze.R_3X3)
                    {
                        b = new SolidBrush(Color.Red);
                    }
                    else if (s == Maze.R_3X4)
                    {
                        b = new SolidBrush(Color.Blue);
                    }
                    else if (s == Maze.R_3X5)
                    {
                        b = new SolidBrush(Color.BlueViolet);
                    }
                    else if (s == Maze.R_3X6)
                    {
                        b = new SolidBrush(Color.Orange);
                    }
                    else if (s == Maze.R_4X4)
                    {
                        b = new SolidBrush(Color.OrangeRed);
                    }
                    else if (s == Maze.R_4X5)
                    {
                        b = new SolidBrush(Color.SeaGreen);
                    }
                    else if (s == Maze.R_4X6)
                    {
                        b = new SolidBrush(Color.DarkGreen);
                    }
                    else if (s == Maze.R_5X5)
                    {
                        b = new SolidBrush(Color.Magenta);
                    }
                    else if (s == Maze.R_5X6)
                    {
                        b = new SolidBrush(Color.Cyan);
                    }
                    else if (s == Maze.R_6X6)
                    {
                        b = new SolidBrush(Color.DarkGoldenrod);
                    }
                    else if (s == Maze.R_ERROR)
                    {
                        b = new SolidBrush(Color.HotPink);           //hot pink is error color
                    }
                    else if (s == Maze.R_FILL)
                    {
                        b = new SolidBrush(Color.YellowGreen);
                    }
                    else if (s == Maze.T_SPIKE)
                    {
                        b = new SolidBrush(Color.DarkOrange);
                        drawStr = Maze.T_SPIKE;
                    }
                    else if (s == Maze.T_STEAM)
                    {
                        b = new SolidBrush(Color.DarkOrange);
                        drawStr = Maze.T_STEAM;
                    }
                    else if (s == Maze.D_START)
                    {
                        b = new SolidBrush(Color.Gold);
                        if (checkTierE.Checked)
                            drawStr += curr.tier_e + "";
                        drawStr += Maze.D_START;
                    }
                    else if (s == Maze.D_GOAL)
                    {
                        b = new SolidBrush(Color.Gold);
                        if(checkTierS.Checked)
                            drawStr += curr.tier_s + "";
                        drawStr += Maze.D_GOAL;
                    }
                    
                    g.FillRectangle(b, (float)(x * spaceWidth), (float)(y * spaceHeight), (float)(spaceWidth), (float)(spaceHeight));
                    if (drawStr != "")
                    {
                        g.DrawString(drawStr, drawFont, txt, (float)((x * spaceWidth)), (float)((y * spaceHeight) + (spaceHeight / 4)));
                    }
                }
            }
            if (drawGridBox.Checked)
            {
                for (int i = 0; i < height + 2; i++)
                {
                    g.DrawLine(p, 0, (float)(i * spaceHeight), (float)drawWidth, (float)(i * spaceHeight));
                }
                for (int i = 0; i < width + 2; i++)
                {
                    g.DrawLine(p, (float)(i * spaceWidth), 0, (float)(i * spaceWidth), (float)(drawHeight));
                }
            }
            //Bitmap bmp = new Bitmap((int)drawWidth, (int)drawHeight, g);
            //bmp.Save("drawnImage.bmp");
            drawFrame.Image = drawArea;
            g.Dispose();
        }

        private void fillMaze(int width, int height, weightedRoom[,] rooms)
        {
            ArrayList possibleRooms = new ArrayList();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)   //work top-left to bottom-right.
                {
                    Point p = new Point(x, y);
                    if (maze.Space(p).Type != Maze.R_EMPTY)  //if this space was already filled, skip it.
                    {
                        continue;
                    }
                    else
                    {
                        int xSpace = calcHorizontalSpaceFrom(p);        //calculate the amount of x-room we have to play with.
                        int ySpace = calcVerticalSpaceFrom(p);  //calculate the amount of y-room we have to play with.
                        if (xSpace == 0 || ySpace == 0)
                            continue;
                        if (xSpace == 1 || ySpace == 1)
                        {
                            MazeSpace hallPlaceholder = new MazeSpace(p, Maze.R_FILL, maze);  //add hall filler. This might get overwritten and will get re-parsed later if it isn't.
                            maze.addSpace(hallPlaceholder);
                            continue;
                        }

                        // check for available x or y space = 2. This is not valid and should be marked for debug.
                        if (xSpace == 2)                                   //this should never happen in this system, 2x? rooms don't exist and 2x1 hallways should have been handled by the previous case.
                        {
                            maze.addRoom(p, 2, 1, Maze.R_ERROR);     // add a 2x1 error room using error symbol '~' for debug.
                            continue;
                        }
                        if (ySpace == 2)
                        {
                            maze.addRoom(p, 1, 2, Maze.R_ERROR);    // add a 1x2 error room using error symbol '~' for debug.
                            continue;
                        }

                        // ------- Enough space to make a room ------- //
                        // Given the amount of space available, only certain types of room may be available while allowing the maze to stay square.
                        // We also want to prevent walls and halls from having double width.
                        // A spreadsheet of available room dimensions for each x/y dimension can be found on Ryan's gDocs.
                        if (xSpace == 3)
                        {
                            if (ySpace == 3)
                            {
                                maze.addRoom(p, 3, 3, Maze.R_3X3);  //only room type available for this combo is 3x3, type 'a'. this skips the rng to save time.
                                continue;
                            }
                            if (ySpace == 4)
                            {
                                maze.addRoom(p, 3, 4, Maze.R_3X4);  //only room type available for this combo is 3x4, type 'b'. this skips the rng to save time.
                                continue;
                            }
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 3, 1))
                                    possibleRooms.Add(rooms[3, 1]);   //possible room types: 3x1 hall 'O'.
                            }
                            if (ySpace == 5 || ySpace >= 7)
                            {
                                possibleRooms.Add(rooms[3, 3]);
                            }
                            if (ySpace == 6 || ySpace >= 8)
                            {
                                possibleRooms.Add(rooms[3, 4]);
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                possibleRooms.Add(rooms[3, 5]);
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace >= 10)
                            {
                                possibleRooms.Add(rooms[3, 6]);
                            }
                        }
                        else if (xSpace == 4)
                        {
                            if (ySpace == 3)
                            {
                                maze.addRoom(p, 4, 3, Maze.R_3X4);  //only room type available for this combo is 4x3, type 'b'. this skips the rng to save time.
                                continue;
                            }
                            if (ySpace == 4)
                            {
                                maze.addRoom(p, 4, 4, Maze.R_4X4);  //only room type available for this combo is 4x4, type 'e'. this skips the rng to save time.
                                continue;
                            }
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 4, 1))
                                    possibleRooms.Add(rooms[4, 1]);//possible room types: 4x1 hall 'O'.
                            }
                            if (ySpace == 5 || ySpace >= 7)
                            {
                                //possible room types: 4x3 room 'b'.
                                possibleRooms.Add(rooms[4, 3]);
                            }
                            if (ySpace == 6 || ySpace >= 8)
                            {
                                //possible room types: 4x4 room 'e'.
                                possibleRooms.Add(rooms[4, 4]);
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                //possible room types: 4x5 room 'f'.
                                possibleRooms.Add(rooms[4, 5]);
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace >= 10)
                            {
                                //possible room types: 3x6 room 'd'.
                                possibleRooms.Add(rooms[4, 6]);
                            }
                        }
                        else if (xSpace == 5)
                        {
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 3, 1))
                                    possibleRooms.Add(rooms[3, 1]);  // 3x1 hall 'O'
                                if (!hasParallel(p, 5, 1))
                                    possibleRooms.Add(rooms[5, 1]);  // 5x1 hall 'O'
                            }
                            if (ySpace == 3 || ySpace == 5 || ySpace >= 7)
                            {
                                if (!hasParallel(p, 1, 3))
                                    possibleRooms.Add(rooms[1, 3]);  //1x3 hall 'O'
                                possibleRooms.Add(rooms[3, 3]);  //3x3 room 'a'
                                possibleRooms.Add(rooms[5, 3]);  //5x3 room 'c'
                            }
                            if (ySpace == 4 || ySpace == 6 || ySpace >= 8)
                            {
                                if (!hasParallel(p, 1, 4))
                                    possibleRooms.Add(rooms[1, 4]);  //1x4 hall 'O'
                                possibleRooms.Add(rooms[3, 4]);  //3x4 room 'b'
                                possibleRooms.Add(rooms[5, 4]);  //5x4 room 'f'
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                if (!hasParallel(p, 1, 5))
                                    possibleRooms.Add(rooms[1, 5]);  //1x5 hall 'O'
                                possibleRooms.Add(rooms[3, 5]);  //3x5 room 'c'
                                possibleRooms.Add(rooms[5, 5]);  //5x5 room 'h'
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace > 10)
                            {
                                if (!hasParallel(p, 1, 6))
                                    possibleRooms.Add(rooms[1, 6]);  //1x6 hall 'O'
                                possibleRooms.Add(rooms[3, 6]);  //3x6 room 'd'
                                possibleRooms.Add(rooms[5, 6]);  //5x6 room 'i'
                            }
                        }
                        else if (xSpace == 6)
                        {
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 4, 1))
                                    possibleRooms.Add(rooms[4, 1]);  //4x1 hall 'O'
                                if (!hasParallel(p, 6, 1))
                                    possibleRooms.Add(rooms[6, 1]);  //6x1 hall 'O'
                            }
                            if (ySpace == 3 || ySpace == 5 || ySpace >= 7)
                            {
                                if (!hasParallel(p, 1, 3))
                                    possibleRooms.Add(rooms[1, 3]);  //1x3 hall 'O'
                                possibleRooms.Add(rooms[4, 3]);  //4x3 room 'b'
                                possibleRooms.Add(rooms[6, 3]);  //6x3 room 'd'
                            }
                            if (ySpace == 4 || ySpace == 6 || ySpace >= 8)
                            {
                                if (!hasParallel(p, 1, 4))
                                    possibleRooms.Add(rooms[1, 4]);  //1x4 hall 'O'
                                possibleRooms.Add(rooms[4, 4]);  //4x4 room 'e'
                                possibleRooms.Add(rooms[6, 4]);  //6x4 room 'g'
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                if (!hasParallel(p, 1, 5))
                                    possibleRooms.Add(rooms[1, 5]);  //1x5 hall 'O'
                                possibleRooms.Add(rooms[4, 5]);  //4x5 room 'f'
                                possibleRooms.Add(rooms[6, 5]);  //6x5 room 'i'
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace > 10)
                            {
                                if (!hasParallel(p, 1, 6))
                                    possibleRooms.Add(rooms[1, 6]);  //1x3 hall 'O'
                                possibleRooms.Add(rooms[4, 6]);  //4x6 room 'g'
                                possibleRooms.Add(rooms[6, 6]);  //6x6 room 'j'
                            }
                        }
                        else if (xSpace == 7)        //this case is the same as xSpace == 5 except with no hall gen chance
                        {
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 3, 1))
                                    possibleRooms.Add(rooms[3, 1]);  //3x1 hall 'O'
                                if (!hasParallel(p, 5, 1))
                                    possibleRooms.Add(rooms[5, 1]);  //5x1 hall 'O'
                            }
                            if (ySpace == 3 || ySpace == 5 || ySpace >= 7)
                            {
                                if (!hasParallel(p, 1, 3))
                                    possibleRooms.Add(rooms[1, 3]);  //1x3 hall 'O'
                                possibleRooms.Add(rooms[3, 3]);  //3x3 room 'a'
                                possibleRooms.Add(rooms[5, 3]);  //5x3 room 'c'
                            }
                            if (ySpace == 4 || ySpace == 6 || ySpace >= 8)
                            {
                                if (!hasParallel(p, 1, 4))
                                    possibleRooms.Add(rooms[1, 4]);  //1x4 hall 'O'
                                possibleRooms.Add(rooms[3, 4]);  //3x4 room 'b'
                                possibleRooms.Add(rooms[5, 4]);  //5x4 room 'f'
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                if (!hasParallel(p, 1, 5))
                                    possibleRooms.Add(rooms[1, 5]);  //1x5 hall 'O'
                                possibleRooms.Add(rooms[3, 5]);  //3x5 room 'c'
                                possibleRooms.Add(rooms[5, 5]);  //5x5 room 'h'
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace > 10)
                            {
                                if (!hasParallel(p, 1, 6))
                                    possibleRooms.Add(rooms[1, 6]);  //1x6 hall 'O'
                                possibleRooms.Add(rooms[3, 6]);  //3x6 room 'd'
                                possibleRooms.Add(rooms[5, 6]);  //5x6 room 'i'
                            }
                        }
                        else if (xSpace == 8)
                        {
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 3, 1))
                                    possibleRooms.Add(rooms[3, 1]);  //3x1 hall 'O'
                                if (!hasParallel(p, 4, 1))
                                    possibleRooms.Add(rooms[4, 1]);  //4x1 hall 'O'
                                if (!hasParallel(p, 6, 1))
                                possibleRooms.Add(rooms[6, 1]);  //6x1 hall 'O'
                            }
                            if (ySpace == 3 || ySpace == 5 || ySpace >= 7)
                            {
                                if (!hasParallel(p, 1, 3))
                                    possibleRooms.Add(rooms[1, 3]);  //1x3 hall 'O'
                                possibleRooms.Add(rooms[3, 3]);  //3x3 room 'a'
                                possibleRooms.Add(rooms[4, 3]);  //4x3 room 'b'
                                possibleRooms.Add(rooms[6, 3]);  //6x3 room 'd'
                            }
                            if (ySpace == 4 || ySpace == 6 || ySpace >= 8)
                            {
                                if (!hasParallel(p, 1, 4))
                                    possibleRooms.Add(rooms[1, 4]);  //1x4 hall 'O'
                                possibleRooms.Add(rooms[3, 4]);  //3x4 room 'b'
                                possibleRooms.Add(rooms[4, 4]);  //4x4 room 'e'
                                possibleRooms.Add(rooms[6, 4]);  //6x4 room 'g'
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                if (!hasParallel(p, 1, 5))
                                    possibleRooms.Add(rooms[1, 5]);  //1x5 hall 'O'
                                possibleRooms.Add(rooms[3, 5]);  //3x5 room 'c'
                                possibleRooms.Add(rooms[4, 5]);  //4x5 room 'f'
                                possibleRooms.Add(rooms[6, 5]);  //6x5 room 'g'
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace > 10)
                            {
                                if (!hasParallel(p, 1, 6))
                                    possibleRooms.Add(rooms[1, 6]);  //1x6 hall 'O'
                                possibleRooms.Add(rooms[3, 6]);  //3x6 room 'd'
                                possibleRooms.Add(rooms[4, 6]);  //4x6 room 'g'
                                possibleRooms.Add(rooms[6, 6]);  //6x6 room 'j'
                            }
                        }
                        else if (xSpace == 9)
                        {
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 3, 1))
                                    possibleRooms.Add(rooms[3, 1]);  //3x1 hall 'O'
                                possibleRooms.Add(rooms[4, 1]);  //4x1 hall 'O'
                                possibleRooms.Add(rooms[5, 1]);  //5x1 hall 'O'
                            }
                            if (ySpace == 3 || ySpace == 5 || ySpace >= 7)
                            {
                                if (!hasParallel(p, 1, 3))
                                    possibleRooms.Add(rooms[1, 3]);  //1x3 hall 'O'
                                possibleRooms.Add(rooms[3, 3]);  //3x3 room 'a'
                                possibleRooms.Add(rooms[4, 3]);  //4x3 room 'b'
                                possibleRooms.Add(rooms[5, 3]);  //5x3 room 'c'
                            }
                            if (ySpace == 4 || ySpace == 6 || ySpace >= 8)
                            {
                                if (!hasParallel(p, 1, 4))
                                    possibleRooms.Add(rooms[1, 4]);  //1x4 hall 'O'
                                possibleRooms.Add(rooms[3, 4]);  //3x4 room 'b'
                                possibleRooms.Add(rooms[4, 4]);  //4x4 room 'e'
                                possibleRooms.Add(rooms[5, 4]);  //5x4 room 'f'
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                if (!hasParallel(p, 1, 5))
                                    possibleRooms.Add(rooms[1, 5]);  //1x5 hall 'O'
                                possibleRooms.Add(rooms[3, 5]);  //3x5 room 'c'
                                possibleRooms.Add(rooms[4, 5]);  //4x5 room 'f'
                                possibleRooms.Add(rooms[5, 5]);  //5x5 room 'h'
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace > 10)
                            {
                                if (!hasParallel(p, 1, 6))
                                    possibleRooms.Add(rooms[1, 6]);  //1x6 hall 'O'
                                possibleRooms.Add(rooms[3, 6]);  //3x6 room 'd'
                                possibleRooms.Add(rooms[4, 6]);  //4x6 room 'g'
                                possibleRooms.Add(rooms[5, 6]);  //5x6 room 'i'
                            }
                        }
                        else if (xSpace >= 10)
                        {
                            if (ySpace >= 5)
                            {
                                if (!hasParallel(p, 3, 1))
                                    possibleRooms.Add(rooms[3, 1]);  //3x1 hall 'O'
                                if (!hasParallel(p, 4, 1))
                                    possibleRooms.Add(rooms[4, 1]);  //4x1 hall 'O'
                                if (!hasParallel(p, 5, 1))
                                    possibleRooms.Add(rooms[5, 1]);  //5x1 hall 'O'
                                if (!hasParallel(p, 6, 1))
                                    possibleRooms.Add(rooms[6, 1]);
                            }
                            if (ySpace == 3 || ySpace == 5 || ySpace >= 7)
                            {
                                if (!hasParallel(p, 1, 3))
                                    possibleRooms.Add(rooms[1, 3]);  //3x1 hall 'O'
                                possibleRooms.Add(rooms[3, 3]);  //3x3 room 'a'
                                possibleRooms.Add(rooms[4, 3]);  //4x3 room 'b'
                                possibleRooms.Add(rooms[5, 3]);  //5x3 room 'c'
                                possibleRooms.Add(rooms[6, 3]);  //6x3 room 'd'
                            }
                            if (ySpace == 4 || ySpace == 6 || ySpace >= 8)
                            {
                                if (!hasParallel(p, 1, 4))
                                    possibleRooms.Add(rooms[1, 4]);  //4x1 hall 'O'
                                possibleRooms.Add(rooms[3, 4]);  //3x4 room 'b'
                                possibleRooms.Add(rooms[4, 4]);  //4x4 room 'e'
                                possibleRooms.Add(rooms[5, 4]);  //5x4 room 'f'
                                possibleRooms.Add(rooms[6, 4]);  //6x4 room 'g'
                            }
                            if (ySpace == 5 || ySpace == 7 || ySpace >= 9)
                            {
                                if (!hasParallel(p, 1, 5))
                                    possibleRooms.Add(rooms[1, 5]);  //5x1 hall 'O'
                                possibleRooms.Add(rooms[3, 5]);  //3x5 room 'c'
                                possibleRooms.Add(rooms[4, 5]);  //4x5 room 'f'
                                possibleRooms.Add(rooms[5, 5]);  //5x5 room 'h'
                                possibleRooms.Add(rooms[6, 5]);  //6x5 room 'g'
                            }
                            if (ySpace == 6 || ySpace == 8 || ySpace > 10)
                            {
                                if(!hasParallel(p, 1, 6))
                                    possibleRooms.Add(rooms[1, 6]);  //6x1 hall 'O'
                                possibleRooms.Add(rooms[3, 6]);  //3x6 room 'd'
                                possibleRooms.Add(rooms[4, 6]);  //4x6 room 'g'
                                possibleRooms.Add(rooms[5, 6]);  //5x6 room 'i'
                                possibleRooms.Add(rooms[6, 6]);  //6x6 room 'j'
                            }
                        }
                        rollAndAdd(possibleRooms, p);     //from the list of possible rooms that we generated, pick one and add it to map at current location (which has been given as [i, j])
                        possibleRooms.Clear();               //clear the list for next iteration
                        continue;
                    }
                }
            }
            //return maze;
        }

        private bool hasParallel(Point p, int w, int h)
        {
            int maxCounts = 0;
            if (w != 1 && h != 1 || w < 1 || h < 1)
            {
                throw new Exception("hall malformed");
            }
            else
            {
                if (w > h)  //checking up/down
                {
                    int upCounts = 0;
                    bool upConsec = false;
                    int downCounts = 0;
                    bool downConsec = false;
                    if (w == 3 || w == 4)
                    {
                        maxCounts = 1;
                    }
                    else
                    {
                        maxCounts = 2;
                    }
                    for (int i = 0; i < w; i++)
                    {
                        MazeSpace m = maze.Space(new Point(p.X + i, p.Y));
                        if (m.Up.Up.Type == Maze.R_HALL)
                        {
                            if (upConsec)
                                return true;
                            else
                            {
                                upConsec = true;
                                upCounts++;
                            }
                        }
                        else
                        {
                            upConsec = false;
                        }
                        if (m.Down.Down.Type == Maze.R_HALL)
                        {
                            if (downConsec)
                                return true;
                            else
                            {
                                downConsec = true;
                                downCounts++;
                            }
                        }
                        else
                        {
                            downConsec = false;
                        }
                    }
                    if (upCounts > maxCounts)
                        return true;
                    if (downCounts > maxCounts)
                        return true;
                }
                else
                {
                    int leftCounts = 0;
                    bool leftConsec = false;
                    int rightCounts = 0;
                    bool rightConsec = false;
                    if (h == 3 || h == 4)
                    {
                        maxCounts = 1;
                    }
                    else
                    {
                        maxCounts = 2;
                    }
                    for (int i = 0; i < h; i++)
                    {
                        MazeSpace m = maze.Space(new Point(p.X, p.Y + i));
                        if (m.Left.Left.Type == Maze.R_HALL)
                        {
                            if (leftConsec)
                                return true;
                            else
                            {
                                leftConsec = true;
                                leftCounts++;
                            }
                        }
                        else
                        {
                            leftConsec = false;
                        }
                        if (m.Right.Right.Type == Maze.R_HALL)
                        {
                            if (rightConsec)
                                return true;
                            else
                            {
                                rightConsec = true;
                                rightCounts++;
                            }
                        }
                        else
                        {
                            rightConsec = false;
                        }
                    }
                    if (leftCounts > maxCounts)
                        return true;
                    if (rightCounts > maxCounts)
                        return true;
                }
            }
            return false;
        }

        private weightedRoom[,] populateRoomArray(weightedRoom[,] roomArray)  //need size [7,7] array (so indices will be up to [6,6]). This is just a function to separate some code.
        {
            bool _3x5 = check3x5.Checked;
            bool _3x6 = check3x6.Checked;
            bool _4x5 = check3x5.Checked;
            bool _4x6 = check3x5.Checked;
            bool _5x5 = check3x5.Checked;
            bool _5x6 = check3x5.Checked;
            bool _6x6 = check3x5.Checked;

            for (int x = 1; x <= 6; x++)
            {
                for (int y = 1; y <= 6; y++)
                {
                    if (x != 2 && y != 2)
                    {
                        double probability = 0;
                        string type = "";
                        if(x == 1 || y == 1)
                        {
                            if (x == 1 && y == 1)
                                probability = (double)chanceHall.Value;
                            else
                                probability = (double)chanceHall.Value / 2;
                            type = Maze.R_HALL;
                        }
                        else if (x == 3)
                        {
                            if (y == 3)
                            {
                                probability = (double)chance3x3.Value;
                                type = Maze.R_3X3;
                            }
                            else if (y == 4)
                            {
                                probability = (double)chance3x4.Value / 2;
                                type = Maze.R_3X4;
                            }
                            else if (y == 5 && _3x5)
                            {
                                probability = (double)chance3x5.Value / 2;
                                type = Maze.R_3X5;
                            }
                            else if (y == 6 && _3x6)
                            {
                                probability = (double)chance3x6.Value / 2;
                                type = Maze.R_3X6;
                            }
                        }
                        else if (x == 4)
                        {
                            if (y == 3)
                            {
                                probability = (double)chance3x4.Value / 2;
                                type = Maze.R_3X4;
                            }
                            else if (y == 4)
                            {
                                probability = (double)chance4x4.Value;
                                type = Maze.R_4X4;
                            }
                            else if (y == 5 && _4x5)
                            {
                                probability = (double)chance4x5.Value / 2;
                                type = Maze.R_4X5;
                            }
                            else if (y == 6 && _4x6)
                            {
                                probability = (double)chance4x6.Value / 2;
                                type = Maze.R_4X6;
                            }
                        }
                        else if (x == 5)
                        {
                            if (y == 3 && _3x5)
                            {
                                probability = (double)chance3x5.Value / 2;
                                type = Maze.R_3X5;
                            }
                            else if (y == 4 && _4x5)
                            {
                                probability = (double)chance4x5.Value / 2;
                                type = Maze.R_4X5;
                            }
                            else if (y == 5 && _5x5)
                            {
                                probability = (double)chance5x5.Value;
                                type = Maze.R_5X5;
                            }
                            else if (y == 6 && _5x6) 
                            {
                                probability = (double)chance5x6.Value / 2;
                                type = Maze.R_5X6;
                            }
                        }
                        else if (x == 6)
                        {
                            if (y == 3 && _3x6)
                            {
                                probability = (double)chance3x6.Value / 2;
                                type = Maze.R_3X6;
                            }
                            else if (y == 4 && _4x6)
                            {
                                probability = (double)chance4x6.Value / 2;
                                type = Maze.R_4X6;
                            }
                            else if (y == 5 && _5x6)
                            {
                                probability = (double)chance5x6.Value / 2;
                                type = Maze.R_5X6;
                            }
                            else if (y == 6 && _6x6)
                            {
                                probability = (double)chance6x6.Value;
                                type = Maze.R_6X6;
                            }
                        }
                        weightedRoom newRoom = new weightedRoom(x, y, probability, type);
                        roomArray[x, y] = newRoom;
                    }
                }
            }
            return roomArray;
        }

        private void rollAndAdd(ArrayList possibleRooms, Point p)
        {
            double maxRoll = 0;
            for (int i = 0; i < possibleRooms.Count; i++)
            {
                maxRoll += ((weightedRoom)possibleRooms[i]).weight;   //add up all the individual room weights
            }
            if (maxRoll > 0)
            {
                double weightindex = 0;
                double roll = rng.NextDouble() * maxRoll;    //generate a random number ('roll') between 0 and the total of the individual weights
                for (int i = 0; i < possibleRooms.Count; i++)
                {
                    weightindex += ((weightedRoom)possibleRooms[i]).weight;  //iterate through each room, adding weight to an index. Compare this to the rolled number. If adding the current room's weight
                                                                             //causes the index to be equal to or greater than the roll, the room got rolled.
                    if (roll <= weightindex)
                    {
                        weightedRoom selectedRoom = (weightedRoom)possibleRooms[i];
                        maze.addRoom(p, selectedRoom.x, selectedRoom.y, selectedRoom.code);
                        return;
                    }
                }
            }
            else
            {
                return;
            }
            throw new Exception("problem in RollAndAdd method");

        }

        /*private void addRoom(int xStart, int yStart, int width, int height, string roomChar)
        {
            List<MazeSpace> spaces = new List<MazeSpace>();
            for (int x = xStart - 1; x <= xStart + width; x++)
            {
                for (int y = yStart - 1; y <= yStart + height; y++)
                {
                    if (x == xStart - 1 || y == yStart - 1 || x == xStart + width || y == yStart + height)
                    {
                        if (mazeSpaces[x, y].type == "." || mazeSpaces[x, y].type == Maze.R_EMPTY)   //if a surrounding space is empty or hall filler, make it a wall
                        {
                            MazeSpace newWall = new MazeSpace(x, y, true, x, y, Maze.W_STD);
                            Maze.addSpace(newWall);
                        }
                    }
                    else
                    {
                        bool isRoot = (x==xStart && y==yStart);
                        MazeSpace newSpace = new MazeSpace(x, y, isRoot, xStart, yStart, roomChar);
                        if (roomChar == Maze.R_HALL)
                        {
                            newSpace.roomIndex = halls.Count;
                        }
                        else
                        {
                            newSpace.roomIndex = mazeRooms.Count;
                        }
                        spaces.Add(newSpace);
                        Maze.addSpace(newSpace);
                    }
                }
            }
            MazeRoom newRoom = new MazeRoom(xStart, yStart, width, height, roomChar);
            newRoom.spaces = spaces;
            if (roomChar == Maze.R_HALL)
            {
                halls.Add(newRoom);
            }
            else
            {
                mazeRooms.Add(newRoom);
            }
        }
        */
        //check each index moving right starting at pos (x, y) for available room.
        private int calcHorizontalSpaceFrom(Point p)  
        {
            int space = 0;
            MazeSpace s = maze.Space(p);
            bool checkRight = true;
            while(checkRight)
            {
                if (s.Type == Maze.R_EMPTY)
                {
                    space++;
                    s = s.Right;
                }
                else
                {
                    checkRight = false;
                }
            }
            return space;
        }

        //check each index moving down starting at pos (x, y) for available room.
        private int calcVerticalSpaceFrom(Point p)
        {
            int space = 0;
            MazeSpace s = maze.Space(p);
            bool checkDown = true;
            while (checkDown)
            {
                if (s.Type == Maze.R_EMPTY)
                {
                    space++;
                    s = s.Down;
                }
                else
                {
                    checkDown = false;
                }
            }
            return space;
        }

        private void drawGridBox_CheckedChanged(object sender, EventArgs e)
        {
            if (generated)
                drawMaze(width, height);
        }
    }
}
