﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGenPrototype
{
    class Point
    {
        private int _x;
        private int _y;
        private bool isNullPoint;

        public Point(int x, int y)
        {
            _x = x;
            _y = y;
            setNullStatus();
        }

        public Point()
        {
            _x = -2;
            _y = -2;
            setNullStatus();
        }

        public Point Left
        {
            get
            {
                int x = _x;
                if (x > -2)
                    x--;
                return new Point(x, _y);
            }
        }

        public Point Right
        {
            get
            {
                if(_x > -2)
                    return new Point(_x+1, _y);
                return new Point();
            }
        }

        public Point Up
        {
            get
            {
                int y = _y;
                if (y > -2)
                    y--;
                return new Point(_x, y);
            }
        }

        public Point Down
        {
            get
            {
                if (_y > -2)
                    return new Point(_x, _y+1);
                return new Point();
            }
        }

        private void setNullStatus()
        {
            if (_x < -1 || _y < -1)
                isNullPoint = true;
            else
                isNullPoint = false;
        }

        public int X
        {
            get { return _x; }
            set { _x = X; setNullStatus(); }
        }
        public int Y
        {
            get { return _y; }
            set { _y = Y; setNullStatus(); }
        }
        public bool Null
        {
            get { return isNullPoint; }
        }
    }
}
