﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGenPrototype
{
    class MazeRoom       //for maze rooms that have already been generated and added to the maze
    {
        private MazeSpace rootSpace;
        private Maze maze;
        private bool hall;
        private int width;
        private int height;
        private string type;
        private Guid _ID;

        private List<MazeSpace> spaces;
        public List<MazeSpace> doors;
        public List<MazeSpace> potentialDoors;

        public int tierToStart;
        public int tierToGoal;

        public MazeRoom(Maze parentMaze)
        {
            _ID = Guid.NewGuid();
            spaces = new List<MazeSpace>();
            doors = new List<MazeSpace>();
            potentialDoors = new List<MazeSpace>();
            maze = parentMaze;
            hall = false;
        }

        public MazeRoom(Point rootPoint, int roomWidth, int roomHeight, string roomType, Maze parentMaze)
        {
            hall = false;
            _ID = Guid.NewGuid();
            spaces = new List<MazeSpace>();
            doors = new List<MazeSpace>();
            potentialDoors = new List<MazeSpace>();
            maze = parentMaze;
            width = roomWidth;
            height = roomHeight;
            MazeSpace root = new MazeSpace(rootPoint, roomType, parentMaze, this);
            rootSpace = root;
            spaces.Add(rootSpace);
            maze.addSpace(rootSpace);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (x == 0 && y == 0)
                        continue;
                    MazeSpace newSpace = new MazeSpace(new Point(rootPoint.X + x, rootPoint.Y + y), roomType, parentMaze, this);
                    spaces.Add(newSpace);
                    maze.addSpace(newSpace);
                }
            }
        }

        public List<MazeSpace> getDoorSpaces()
        {
            List<MazeSpace> doorSpaces = new List<MazeSpace>();
            for (int x = Root.X; x < Root.X + width; x++)
            {
                MazeSpace hi = maze.Space(new Point(x, Root.Y - 1));
                MazeSpace lo = maze.Space(new Point(x, Root.Y + height));
                if (maze.roomTypes.Contains(hi.Up.Type))
                {
                    doorSpaces.Add(hi);
                }
                if (maze.roomTypes.Contains(lo.Down.Type))
                {
                    doorSpaces.Add(lo);
                }
            }
            for (int y = Root.Y; y < Root.Y + height; y++)
            {
                MazeSpace left = maze.Space(new Point(Root.X - 1, y));
                MazeSpace right = maze.Space(new Point(Root.X + width, y));
                if (maze.roomTypes.Contains(left.Left.Type))
                {
                    doorSpaces.Add(left);
                }
                if (maze.roomTypes.Contains(right.Right.Type))
                {
                    doorSpaces.Add(right);
                }
            }
            potentialDoors = doorSpaces;
            return doorSpaces;
        }

        public MazeRoom(List<MazeSpace> ConsecHallSpaces, MazeSpace root)
        {
            hall = true;
            spaces = ConsecHallSpaces;
            doors = new List<MazeSpace>();
            potentialDoors = new List<MazeSpace>();
            rootSpace = root;
            maze = root.Maze;
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public void Merge(MazeRoom sourceRoom)
        {
            if(sourceRoom.ID == ID)
            {
                throw new Exception("Source room same as destination room");
            }
            int numRooms = sourceRoom.Spaces.Count;
            for (int i = 0; i < numRooms; i++)
            {
                AddSpace(sourceRoom.RemoveSpace(sourceRoom.Spaces[0]));
            }
        }

        public Guid ID
        {
            get { return _ID; }
        }

        public List<MazeSpace> Spaces
        {
            get { return spaces; }
        }

        public MazeSpace Root
        {
            get { return rootSpace; }
        }

        public bool Contains(MazeSpace m)
        {
            for (int i = 0; i < spaces.Count; i++)
            {
                if (m.ID == spaces[i].ID)
                    return true;
            }
            return false;
        }

        public MazeSpace RemoveSpace(MazeSpace m)
        {
            if (!Contains(m))
                throw new Exception("Space does not exist in room");
            else
            {
                m.Room = maze.NullRoom;
                spaces.Remove(m);
            }
            return m;
        }

        public void AddSpace(MazeSpace m)
        {
            if (Contains(m))
                return;
            else
            {
                m.Room = this;
                spaces.Add(m);
            }
        }

        /*public bool addHallSpace(MazeSpace m)
        {
            spaces.Add(m);
            int actions = 0;
            if (xTopLeft != m.x)
                actions += 1;
            if (yTopLeft != m.y)
                actions += 2;
            if (actions == 1)
                width++;
            else if (actions == 2)
                height++;
            else if (actions == 3)
                return true;
            else if (actions == 0)
                return true;
            return false;
        }*/
    }
}
