﻿namespace MazeGenPrototype
{
    partial class GenerateView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.widthNum = new System.Windows.Forms.NumericUpDown();
            this.heightNum = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.genButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.chanceHall = new System.Windows.Forms.NumericUpDown();
            this.chance3x3 = new System.Windows.Forms.NumericUpDown();
            this.chance3x4 = new System.Windows.Forms.NumericUpDown();
            this.chance3x5 = new System.Windows.Forms.NumericUpDown();
            this.chance3x6 = new System.Windows.Forms.NumericUpDown();
            this.chance4x4 = new System.Windows.Forms.NumericUpDown();
            this.chance4x5 = new System.Windows.Forms.NumericUpDown();
            this.chance4x6 = new System.Windows.Forms.NumericUpDown();
            this.chance5x5 = new System.Windows.Forms.NumericUpDown();
            this.chance5x6 = new System.Windows.Forms.NumericUpDown();
            this.chance6x6 = new System.Windows.Forms.NumericUpDown();
            this.drawFrame = new System.Windows.Forms.PictureBox();
            this.hallMonitor = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.drawGridBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.trapCheck = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.RoomMonitor = new System.Windows.Forms.ListBox();
            this.roomCounter = new System.Windows.Forms.Label();
            this.trapRate = new System.Windows.Forms.NumericUpDown();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.check6x6 = new System.Windows.Forms.CheckBox();
            this.check5x6 = new System.Windows.Forms.CheckBox();
            this.check5x5 = new System.Windows.Forms.CheckBox();
            this.check4x6 = new System.Windows.Forms.CheckBox();
            this.check4x5 = new System.Windows.Forms.CheckBox();
            this.check3x6 = new System.Windows.Forms.CheckBox();
            this.check3x5 = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.checkTierS = new System.Windows.Forms.CheckBox();
            this.checkTierE = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.widthNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chanceHall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance4x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance4x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance4x6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance5x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance5x6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance6x6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trapRate)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // widthNum
            // 
            this.widthNum.Location = new System.Drawing.Point(78, 6);
            this.widthNum.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.widthNum.Name = "widthNum";
            this.widthNum.Size = new System.Drawing.Size(53, 20);
            this.widthNum.TabIndex = 0;
            this.widthNum.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // heightNum
            // 
            this.heightNum.Location = new System.Drawing.Point(78, 32);
            this.heightNum.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.heightNum.Name = "heightNum";
            this.heightNum.Size = new System.Drawing.Size(53, 20);
            this.heightNum.TabIndex = 1;
            this.heightNum.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Width <--->";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Height ^ v";
            // 
            // genButton
            // 
            this.genButton.Location = new System.Drawing.Point(721, 691);
            this.genButton.Name = "genButton";
            this.genButton.Size = new System.Drawing.Size(75, 23);
            this.genButton.TabIndex = 14;
            this.genButton.Text = "Generate";
            this.genButton.UseVisualStyleBackColor = true;
            this.genButton.Click += new System.EventHandler(this.genButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Dimensions   |  Ratio";
            // 
            // chanceHall
            // 
            this.chanceHall.Location = new System.Drawing.Point(97, 135);
            this.chanceHall.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chanceHall.Name = "chanceHall";
            this.chanceHall.Size = new System.Drawing.Size(34, 20);
            this.chanceHall.TabIndex = 18;
            this.chanceHall.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // chance3x3
            // 
            this.chance3x3.Location = new System.Drawing.Point(97, 161);
            this.chance3x3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance3x3.Name = "chance3x3";
            this.chance3x3.Size = new System.Drawing.Size(34, 20);
            this.chance3x3.TabIndex = 19;
            this.chance3x3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance3x4
            // 
            this.chance3x4.Location = new System.Drawing.Point(97, 187);
            this.chance3x4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance3x4.Name = "chance3x4";
            this.chance3x4.Size = new System.Drawing.Size(34, 20);
            this.chance3x4.TabIndex = 20;
            this.chance3x4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance3x5
            // 
            this.chance3x5.Location = new System.Drawing.Point(97, 213);
            this.chance3x5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance3x5.Name = "chance3x5";
            this.chance3x5.Size = new System.Drawing.Size(34, 20);
            this.chance3x5.TabIndex = 21;
            this.chance3x5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance3x6
            // 
            this.chance3x6.Location = new System.Drawing.Point(97, 239);
            this.chance3x6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance3x6.Name = "chance3x6";
            this.chance3x6.Size = new System.Drawing.Size(34, 20);
            this.chance3x6.TabIndex = 22;
            this.chance3x6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance4x4
            // 
            this.chance4x4.Location = new System.Drawing.Point(97, 265);
            this.chance4x4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance4x4.Name = "chance4x4";
            this.chance4x4.Size = new System.Drawing.Size(34, 20);
            this.chance4x4.TabIndex = 23;
            this.chance4x4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance4x5
            // 
            this.chance4x5.Location = new System.Drawing.Point(97, 291);
            this.chance4x5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance4x5.Name = "chance4x5";
            this.chance4x5.Size = new System.Drawing.Size(34, 20);
            this.chance4x5.TabIndex = 24;
            this.chance4x5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance4x6
            // 
            this.chance4x6.Location = new System.Drawing.Point(97, 317);
            this.chance4x6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance4x6.Name = "chance4x6";
            this.chance4x6.Size = new System.Drawing.Size(34, 20);
            this.chance4x6.TabIndex = 25;
            this.chance4x6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance5x5
            // 
            this.chance5x5.Location = new System.Drawing.Point(97, 343);
            this.chance5x5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance5x5.Name = "chance5x5";
            this.chance5x5.Size = new System.Drawing.Size(34, 20);
            this.chance5x5.TabIndex = 26;
            this.chance5x5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance5x6
            // 
            this.chance5x6.Location = new System.Drawing.Point(97, 369);
            this.chance5x6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance5x6.Name = "chance5x6";
            this.chance5x6.Size = new System.Drawing.Size(34, 20);
            this.chance5x6.TabIndex = 27;
            this.chance5x6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chance6x6
            // 
            this.chance6x6.Location = new System.Drawing.Point(97, 395);
            this.chance6x6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chance6x6.Name = "chance6x6";
            this.chance6x6.Size = new System.Drawing.Size(34, 20);
            this.chance6x6.TabIndex = 28;
            this.chance6x6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // drawFrame
            // 
            this.drawFrame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawFrame.Location = new System.Drawing.Point(12, 12);
            this.drawFrame.Name = "drawFrame";
            this.drawFrame.Size = new System.Drawing.Size(650, 650);
            this.drawFrame.TabIndex = 29;
            this.drawFrame.TabStop = false;
            // 
            // hallMonitor
            // 
            this.hallMonitor.FormattingEnabled = true;
            this.hallMonitor.Location = new System.Drawing.Point(9, 136);
            this.hallMonitor.Name = "hallMonitor";
            this.hallMonitor.Size = new System.Drawing.Size(219, 121);
            this.hallMonitor.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Halls";
            // 
            // drawGridBox
            // 
            this.drawGridBox.AutoSize = true;
            this.drawGridBox.Location = new System.Drawing.Point(6, 6);
            this.drawGridBox.Name = "drawGridBox";
            this.drawGridBox.Size = new System.Drawing.Size(73, 17);
            this.drawGridBox.TabIndex = 32;
            this.drawGridBox.Text = "Draw Grid";
            this.drawGridBox.UseVisualStyleBackColor = true;
            this.drawGridBox.CheckedChanged += new System.EventHandler(this.drawGridBox_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Hall";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "3x3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "3x4";
            // 
            // trapCheck
            // 
            this.trapCheck.AutoSize = true;
            this.trapCheck.Location = new System.Drawing.Point(4, 59);
            this.trapCheck.Name = "trapCheck";
            this.trapCheck.Size = new System.Drawing.Size(70, 17);
            this.trapCheck.TabIndex = 44;
            this.trapCheck.Text = "Hall traps";
            this.trapCheck.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 273);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "Rooms:";
            // 
            // RoomMonitor
            // 
            this.RoomMonitor.FormattingEnabled = true;
            this.RoomMonitor.Location = new System.Drawing.Point(9, 289);
            this.RoomMonitor.Name = "RoomMonitor";
            this.RoomMonitor.Size = new System.Drawing.Size(219, 121);
            this.RoomMonitor.TabIndex = 46;
            // 
            // roomCounter
            // 
            this.roomCounter.AutoSize = true;
            this.roomCounter.Location = new System.Drawing.Point(66, 273);
            this.roomCounter.Name = "roomCounter";
            this.roomCounter.Size = new System.Drawing.Size(13, 13);
            this.roomCounter.TabIndex = 47;
            this.roomCounter.Text = "0";
            // 
            // trapRate
            // 
            this.trapRate.DecimalPlaces = 3;
            this.trapRate.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.trapRate.Location = new System.Drawing.Point(78, 58);
            this.trapRate.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.trapRate.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.trapRate.Name = "trapRate";
            this.trapRate.Size = new System.Drawing.Size(53, 20);
            this.trapRate.TabIndex = 48;
            this.trapRate.Value = new decimal(new int[] {
            115,
            0,
            0,
            196608});
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(668, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(242, 616);
            this.tabControl1.TabIndex = 49;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.check6x6);
            this.tabPage1.Controls.Add(this.check5x6);
            this.tabPage1.Controls.Add(this.check5x5);
            this.tabPage1.Controls.Add(this.check4x6);
            this.tabPage1.Controls.Add(this.check4x5);
            this.tabPage1.Controls.Add(this.check3x6);
            this.tabPage1.Controls.Add(this.check3x5);
            this.tabPage1.Controls.Add(this.widthNum);
            this.tabPage1.Controls.Add(this.trapRate);
            this.tabPage1.Controls.Add(this.heightNum);
            this.tabPage1.Controls.Add(this.trapCheck);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.chanceHall);
            this.tabPage1.Controls.Add(this.chance3x3);
            this.tabPage1.Controls.Add(this.chance3x4);
            this.tabPage1.Controls.Add(this.chance3x5);
            this.tabPage1.Controls.Add(this.chance3x6);
            this.tabPage1.Controls.Add(this.chance4x4);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.chance4x5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.chance4x6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.chance5x5);
            this.tabPage1.Controls.Add(this.chance5x6);
            this.tabPage1.Controls.Add(this.chance6x6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(234, 590);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Generator settings";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 267);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "4x4";
            // 
            // check6x6
            // 
            this.check6x6.AutoSize = true;
            this.check6x6.Checked = true;
            this.check6x6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check6x6.Location = new System.Drawing.Point(6, 387);
            this.check6x6.Name = "check6x6";
            this.check6x6.Size = new System.Drawing.Size(43, 17);
            this.check6x6.TabIndex = 56;
            this.check6x6.Text = "6x6";
            this.check6x6.UseVisualStyleBackColor = true;
            // 
            // check5x6
            // 
            this.check5x6.AutoSize = true;
            this.check5x6.Checked = true;
            this.check5x6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check5x6.Location = new System.Drawing.Point(6, 361);
            this.check5x6.Name = "check5x6";
            this.check5x6.Size = new System.Drawing.Size(43, 17);
            this.check5x6.TabIndex = 55;
            this.check5x6.Text = "5x6";
            this.check5x6.UseVisualStyleBackColor = true;
            // 
            // check5x5
            // 
            this.check5x5.AutoSize = true;
            this.check5x5.Checked = true;
            this.check5x5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check5x5.Location = new System.Drawing.Point(6, 338);
            this.check5x5.Name = "check5x5";
            this.check5x5.Size = new System.Drawing.Size(43, 17);
            this.check5x5.TabIndex = 54;
            this.check5x5.Text = "5x5";
            this.check5x5.UseVisualStyleBackColor = true;
            // 
            // check4x6
            // 
            this.check4x6.AutoSize = true;
            this.check4x6.Checked = true;
            this.check4x6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check4x6.Location = new System.Drawing.Point(6, 312);
            this.check4x6.Name = "check4x6";
            this.check4x6.Size = new System.Drawing.Size(43, 17);
            this.check4x6.TabIndex = 53;
            this.check4x6.Text = "4x6";
            this.check4x6.UseVisualStyleBackColor = true;
            // 
            // check4x5
            // 
            this.check4x5.AutoSize = true;
            this.check4x5.Checked = true;
            this.check4x5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check4x5.Location = new System.Drawing.Point(6, 289);
            this.check4x5.Name = "check4x5";
            this.check4x5.Size = new System.Drawing.Size(43, 17);
            this.check4x5.TabIndex = 52;
            this.check4x5.Text = "4x5";
            this.check4x5.UseVisualStyleBackColor = true;
            // 
            // check3x6
            // 
            this.check3x6.AutoSize = true;
            this.check3x6.Checked = true;
            this.check3x6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check3x6.Location = new System.Drawing.Point(6, 240);
            this.check3x6.Name = "check3x6";
            this.check3x6.Size = new System.Drawing.Size(43, 17);
            this.check3x6.TabIndex = 50;
            this.check3x6.Text = "3x6";
            this.check3x6.UseVisualStyleBackColor = true;
            // 
            // check3x5
            // 
            this.check3x5.AutoSize = true;
            this.check3x5.Checked = true;
            this.check3x5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check3x5.Location = new System.Drawing.Point(6, 214);
            this.check3x5.Name = "check3x5";
            this.check3x5.Size = new System.Drawing.Size(43, 17);
            this.check3x5.TabIndex = 49;
            this.check3x5.Text = "3x5";
            this.check3x5.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkTierE);
            this.tabPage2.Controls.Add(this.checkTierS);
            this.tabPage2.Controls.Add(this.drawGridBox);
            this.tabPage2.Controls.Add(this.hallMonitor);
            this.tabPage2.Controls.Add(this.roomCounter);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.RoomMonitor);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(234, 590);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Current view";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(815, 639);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 50;
            this.button1.Text = "roll new maze";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.genButton_Click);
            // 
            // checkTierS
            // 
            this.checkTierS.AutoSize = true;
            this.checkTierS.Checked = true;
            this.checkTierS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkTierS.Location = new System.Drawing.Point(6, 29);
            this.checkTierS.Name = "checkTierS";
            this.checkTierS.Size = new System.Drawing.Size(109, 17);
            this.checkTierS.TabIndex = 48;
            this.checkTierS.Text = "Draw Tier to Start";
            this.checkTierS.UseVisualStyleBackColor = true;
            // 
            // checkTierE
            // 
            this.checkTierE.AutoSize = true;
            this.checkTierE.Checked = true;
            this.checkTierE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkTierE.Location = new System.Drawing.Point(6, 52);
            this.checkTierE.Name = "checkTierE";
            this.checkTierE.Size = new System.Drawing.Size(106, 17);
            this.checkTierE.TabIndex = 49;
            this.checkTierE.Text = "Draw Tier to End";
            this.checkTierE.UseVisualStyleBackColor = true;
            // 
            // GenerateView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 666);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.drawFrame);
            this.Controls.Add(this.genButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "GenerateView";
            this.Text = "MazeGen Prototype Visualizer";
            ((System.ComponentModel.ISupportInitialize)(this.widthNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chanceHall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance3x6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance4x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance4x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance4x6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance5x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance5x6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chance6x6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trapRate)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown widthNum;
        private System.Windows.Forms.NumericUpDown heightNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button genButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown chanceHall;
        private System.Windows.Forms.NumericUpDown chance3x3;
        private System.Windows.Forms.NumericUpDown chance3x4;
        private System.Windows.Forms.NumericUpDown chance3x5;
        private System.Windows.Forms.NumericUpDown chance3x6;
        private System.Windows.Forms.NumericUpDown chance4x4;
        private System.Windows.Forms.NumericUpDown chance4x5;
        private System.Windows.Forms.NumericUpDown chance4x6;
        private System.Windows.Forms.NumericUpDown chance5x5;
        private System.Windows.Forms.NumericUpDown chance5x6;
        private System.Windows.Forms.NumericUpDown chance6x6;
        private System.Windows.Forms.PictureBox drawFrame;
        private System.Windows.Forms.ListBox hallMonitor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox drawGridBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox trapCheck;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ListBox RoomMonitor;
        private System.Windows.Forms.Label roomCounter;
        private System.Windows.Forms.NumericUpDown trapRate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox check6x6;
        private System.Windows.Forms.CheckBox check5x6;
        private System.Windows.Forms.CheckBox check5x5;
        private System.Windows.Forms.CheckBox check4x6;
        private System.Windows.Forms.CheckBox check4x5;
        private System.Windows.Forms.CheckBox check3x6;
        private System.Windows.Forms.CheckBox check3x5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkTierS;
        private System.Windows.Forms.CheckBox checkTierE;
    }
}

