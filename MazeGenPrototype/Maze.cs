﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MazeGenPrototype
{
    class Maze
    {        
        /*        MAZE SPACE TYPES         */

        //code: R_   -> Room space type
        //      W_   -> Wall space type
        //      T_   -> Trap space type
        //      D_   -> Door
        //         : Door will be generated as D_STD and given subtype during pathing phase
            
          //construction
        public const string W_STD = "W";
        public const string W_INV = "X";
        public const string R_HALL = "O";
        public const string R_3X3 = "a";
        public const string R_3X4 = "b";
        public const string R_3X5 = "c";
        public const string R_3X6 = "d";
        public const string R_4X4 = "e";
        public const string R_4X5 = "f";
        public const string R_4X6 = "g";
        public const string R_5X5 = "h";
        public const string R_5X6 = "i";
        public const string R_6X6 = "j";

          //doors
        public const string D_START = "!";
        public const string D_GOAL = "@";
        public const string D_STD = "D";
          //traps
        public const string T_SPIKE = "^";
        public const string T_STEAM = "s";

          //error types
        public const string R_EMPTY = "/";
        public const string R_ERROR = "~";
        public const string R_FILL = ".";


        public List<string> roomTypes;
        public List<string> wallTypes;
        public List<string> trapTypes;
        public List<string> doorTypes;

        private int totalwidth;
        private int totalheight;
        private int interiorwidth;
        private int interiorheight;

        private MazeSpace[,] spaces;
        private MazeSpace errorSpace;
        private MazeRoom errorRoom;

        private List<MazeRoom> halls;
        private List<MazeRoom> rooms;
        private List<MazeSpace> traps;

        public MazeSpace start;
        public MazeSpace goal;

        public Maze(int width, int height)
        {
            interiorwidth = width;
            interiorheight = height;
            totalwidth = width+2;
            totalheight = height+2;

            populateTypeLists();

            spaces = new MazeSpace[totalwidth, totalheight];

            errorRoom = new MazeRoom(this);
            errorSpace = new MazeSpace(new Point(), R_ERROR, this);
            
            halls = new List<MazeRoom>();
            rooms = new List<MazeRoom>();
            traps = new List<MazeSpace>();

            for(int x=0; x<totalwidth; x++)
            {
                spaces[x, 0] = new MazeSpace(new Point(x-1, -1), W_INV, this);
                spaces[x, totalheight-1] = new MazeSpace(new Point(x-1, height), W_INV, this);
            }
            for (int y = 1; y < totalheight-1; y++)
            {
                spaces[0, y] = new MazeSpace(new Point(-1, y - 1), W_INV, this);
                spaces[totalwidth - 1, y] = new MazeSpace(new Point(width, y - 1), W_INV, this);
            }

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    spaces[x + 1, y + 1] = new MazeSpace(new Point(x, y), R_EMPTY, this);
                }
            }

        }

        public MazeRoom NullRoom { get { return errorRoom; } }
        public MazeSpace NullSpace { get { return errorSpace; } }

        public List<MazeRoom> Halls { get { return halls; } }
        public List<MazeRoom> Rooms { get { return rooms; } }

        private void populateTypeLists()
        {
            roomTypes = new List<string>();
            roomTypes.Add(R_HALL);
            roomTypes.Add(R_3X3);
            roomTypes.Add(R_3X4);
            roomTypes.Add(R_3X5);
            roomTypes.Add(R_3X6);
            roomTypes.Add(R_4X4);
            roomTypes.Add(R_4X5);
            roomTypes.Add(R_4X6);
            roomTypes.Add(R_5X5);
            roomTypes.Add(R_5X6);
            roomTypes.Add(R_6X6);

            wallTypes = new List<string>();
            wallTypes.Add(W_STD);
            wallTypes.Add(W_INV);

            trapTypes = new List<string>();
            trapTypes.Add(T_SPIKE);
            trapTypes.Add(T_STEAM);

            doorTypes = new List<string>();
            doorTypes.Add(D_STD);
            doorTypes.Add(D_START);
            doorTypes.Add(D_GOAL);
        }

        public void addRoom(Point p, int width, int height, string type)
        {
            MazeRoom newRoom = new MazeRoom(p, width, height, type, this);
            if (width == 1 || height == 1)
                halls.Add(newRoom);
            else
                rooms.Add(newRoom);
            for (int x = p.X; x <= p.X + width+1; x++)
            {
                if (spaces[x, p.Y].Type == R_EMPTY || spaces[x, p.Y].Up.Type == R_FILL)
                {
                    MazeSpace W = new MazeSpace(new Point(x-1, p.Y-1), W_STD, this);
                    addSpace(W);
                }
                if (spaces[x, p.Y + height + 1].Type == R_EMPTY || spaces[x, p.Y + height + 1].Type == R_FILL)
                {
                    MazeSpace W = new MazeSpace(new Point(x-1, p.Y + height), W_STD, this);
                    addSpace(W);
                }
            }
            for (int y = p.Y; y <= p.Y + height + 1; y++)
            {
                if (spaces[p.X, y].Type == R_EMPTY || spaces[p.X, y].Type == R_FILL)
                {
                    MazeSpace W = new MazeSpace(new Point(p.X-1, y-1), W_STD, this);
                    addSpace(W);
                }
                if (spaces[p.X + width + 1, y].Type == R_EMPTY || spaces[p.X + width + 1, y].Type == R_FILL)
                {
                    MazeSpace W = new MazeSpace(new Point(p.X + width, y-1), W_STD, this);
                    addSpace(W);
                }
            }
        }

        public void addRoom(MazeRoom r)
        {
            rooms.Add(r);
        }

        public void addHall(MazeRoom h)
        {
            halls.Add(h);
        }

        public void deleteRoom(MazeRoom r)
        {
            if (r.Spaces.Count == 0)
            {
                rooms.Remove(r);
            }
        }

        public void deleteHall(MazeRoom r)
        {
            if (r.Spaces.Count == 0)
            {
                halls.Remove(r);
            }
        }
        /*

        //List<MazeSpace> spaces = new List<MazeSpace>();
        for (int x = xStart - 1; x <= xStart + width; x++)
        {
            for (int y = yStart - 1; y <= yStart + height; y++)
            {
                if (x == xStart - 1 || y == yStart - 1 || x == xStart + width || y == yStart + height)
                {
                    if (mazeSpaces[x, y].type == "." || mazeSpaces[x, y].type == Maze.R_EMPTY)   //if a surrounding space is empty or hall filler, make it a wall
                    {
                        MazeSpace newWall = new MazeSpace(x, y, true, x, y, Maze.W_STD);
                        Maze.addSpace(newWall);
                    }
                }
                else
                {
                    bool isRoot = (x == xStart && y == yStart);
                    MazeSpace newSpace = new MazeSpace(x, y, isRoot, xStart, yStart, roomChar);
                    if (roomChar == Maze.R_HALL)
                    {
                        newSpace.roomIndex = halls.Count;
                    }
                    else
                    {
                        newSpace.roomIndex = mazeRooms.Count;
                    }
                    spaces.Add(newSpace);
                    Maze.addSpace(newSpace);
                }
            }
        }
        MazeRoom newRoom = new MazeRoom(xStart, yStart, width, height, roomChar);
        newRoom.spaces = spaces;
        if (roomChar == Maze.R_HALL)
        {
            halls.Add(newRoom);
        }
        else
        {
            mazeRooms.Add(newRoom);
        }
    }*/

        public void addSpace(MazeSpace s)
        {
            s.Maze = this;
            spaces[s.X + 1, s.Y + 1] = s;
        }

        public void addTrap(MazeSpace s, string trapType)
        {
            setSpace(s, trapType);
            traps.Add(s);
        }

        public void setSpace(MazeSpace s, string spaceType)
        {
            s.Type = spaceType;
        }

        //maze interior only, (0,0) is top left space
        public MazeSpace Space(Point p)
        {
            int x = p.X;
            int y = p.Y;
            if (x < -1 || x > interiorwidth  || y < -1 || y > interiorheight)
                return errorSpace;
            return spaces[x+1, y+1];
        }

        public void WriteToFile(string filename)
        {
            string s = "";
            for (int y = 0; y < totalheight; y++)
            {
                for (int x = 0; x < totalwidth; x++)
                {
                    s += spaces[x, y].Type + "";
                }
                s += System.Environment.NewLine;
            }

            File.WriteAllText(filename, s);
        }

        /* Use Maze.BorderDirection for BorderIndex term*/
        /*public MazeSpace BorderSpace(int dim, int BorderIndex)
        {
            if (dim < 0)
                return errorSpace;
            if (BorderIndex == TOP)
            {
                if (dim >= interiorwidth)
                    return errorSpace;
                return spaces[dim + 1, 0];
            }
            else if (BorderIndex == LEFT)
            {
                if (dim >= interiorheight)
                    return errorSpace;
                return spaces[0, dim + 1];
            }
            else if (BorderIndex == RIGHT)
            {
                if (dim >= interiorheight)
                    return errorSpace;
                return spaces[totalwidth - 1, dim + 1];
            }
            else if (BorderIndex == BOTTOM)
            {
                if (dim >= interiorwidth)
                    return errorSpace;
                return spaces[totalheight - 1, dim + 1];
            }
            return errorSpace;
        }*/

    }
}
