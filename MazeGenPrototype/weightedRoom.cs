﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGenPrototype
{
    class weightedRoom
    {
        public int x;
        public int y;
        public double weight;
        public string code;

        public weightedRoom(int xSize, int ySize, double probability, string type)
        {
            x = xSize;
            y = ySize;
            weight = probability;
            code = type;
        }
    }
}
